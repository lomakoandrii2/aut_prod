from base.selenium_base import SeleniumBase
from base.locators import CartPageLocators


class CartPage(SeleniumBase):

    def empty_cart_text_is_visible(self):
        return self.is_visible(*CartPageLocators.EMPTY_CART_TEXT)

    def emty_cart_link_in_text_is_visible(self):
        return self.is_visible(*CartPageLocators.EMPTY_CART_TEXT_LINK)

    def qty_field_isvisible(self):
        return self.is_visible(*CartPageLocators.QTY_FIELD)

    def subtotal_price_is_visible(self):
        return self.is_visible(*CartPageLocators.SUBTOTAL_PRICE)
