from base.selenium_base import SeleniumBase
from base.locators import MmyPageLocators


class MmyPage(SeleniumBase):

    def select_make_is_visible(self):
        return self.is_visible(*MmyPageLocators.SELECT_MAKE_DROPDOWN)

    def selector_dropdown_list_values_text(self, expected):
        return self.is_text_in_element_present(*MmyPageLocators.SELECT_MAKE_DROPDOWN, expected)