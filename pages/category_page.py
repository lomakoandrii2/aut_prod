from base.selenium_base import SeleniumBase
from base.locators import CategoryPageLocators


class CategoryPage(SeleniumBase):

    def home_is_visible(self):
        return self.is_visible(*CategoryPageLocators.HOME_BREADCRUMBS)
