from base.selenium_base import SeleniumBase
from base.locators import ProductPageLocators


class ProductPage(SeleniumBase):

    def is_hreflag_present(self):
        hreflang_en = self.is_present(*ProductPageLocators.HREFLANG_EN)
        return hreflang_en

    def is_canonical_present(self):
        canonical = self.is_present(*ProductPageLocators.CANONICAL)
        return canonical

    def home_is_visible(self):
        return self.is_visible(*ProductPageLocators.HOME_BREADCRUMBS)

    def simple_is_visible(self):
        return self.is_visible(*ProductPageLocators.SIMPLE)

    def add_to_cart_button_is_visible(self):
        return self.is_visible(*ProductPageLocators.ADD_TO_CART_BUTTON)

    def cart_is_visible(self):
        return self.is_visible(*ProductPageLocators.CART)

    def counter_number_in_cart_is_visible(self, expected_text):
        return self.is_text_in_element_present(*ProductPageLocators.COUNTER_NUMBER_MINI_CART, expected_text)

    def view_basket_button_is_visible(self):
        return self.is_visible(*ProductPageLocators.VIEW_AND_EDIT_BASKET_BUTTON)

    def product_price_is_visible(self):
        return self.is_visible(*ProductPageLocators.PRODUCT_PRICE)

    def minicart_price_is_visible(self):
        return self.is_visible(*ProductPageLocators.MINICART_PRICE)

    def success_message_is_visible(self):
        return self.is_visible(*ProductPageLocators.SUCCESS_MESSAGE)