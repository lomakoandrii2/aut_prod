from base.links import Links
from pages.home_page import HomePage
import pytest
from base.data_for_test import ExpectedData
from base.locators import HomePageLocators as HPL


class TestHomePage:

    @pytest.mark.parametrize("link,current,expected", [(link, HPL.CATEGORY_BANNERS_H3, expected) for link, expected in zip(Links.HOME_PAGES, ExpectedData.CATEGORY_BANNERS_H3)])
    def test_category_banners_h3(self, driver, link, current, expected):
        page = HomePage(driver, link)
        page.open()
        page.category_banners_h3_are_correct(current, expected)

    @pytest.mark.parametrize("link,current,expected", [(link, HPL.CATEGORY_BANNERS_DESCRIPTION, expected) for link, expected in zip(Links.HOME_PAGES, ExpectedData.CATEGORY_BANNERS_DESCRIPTIONS)])
    def test_category_banners_description(self, driver, link, current, expected):
        page = HomePage(driver, link)
        page.open()
        page.category_banners_description_are_correct(current, expected)

    @pytest.mark.parametrize("link,expected", [(link, expected) for link, expected in zip(Links.HOME_PAGES, ExpectedData.MODEL_SELECT_DROPDOWN_TEXT)])
    def test_model_selector(self, driver, link, expected):
        page = HomePage(driver, link)
        page.open()
        page.model_selector_dropdown_list_values_text(expected)

    @pytest.mark.parametrize("link,expected", [(link, expected) for link, expected in zip(Links.HOME_PAGES, ExpectedData.MODEL_SELECT_DROPDOWN_TEXT)])
    def test_vehicle_selector(self, driver, link, expected):
        page = HomePage(driver, link)
        page.open()
        page.shop_your_vehicle_is_visible().click()
        page.model_selector_dropdown_list_values_text(expected)





