import pytest
from base.links import Links
from pages.mmy_page import MmyPage
from base.data_for_test import ExpectedData


class TestMmyPage:

    @pytest.mark.parametrize("link", Links.MMY_PAGES[0:7])
    def test_select_make_en(self, driver, link):
        page = MmyPage(driver, link)
        page.open()
        page.select_make_is_visible()
        page.selector_dropdown_list_values_text(ExpectedData.MODEL_SELECT_DROPDOWN_TEXT[0])

    @pytest.mark.parametrize("link", Links.MMY_PAGES[7:14])
    def test_select_make_fr(self, driver, link):
        page = MmyPage(driver, link)
        page.open()
        page.select_make_is_visible()
        page.selector_dropdown_list_values_text(ExpectedData.MODEL_SELECT_DROPDOWN_TEXT[1])

    @pytest.mark.parametrize("link", Links.MMY_PAGES[14:21])
    def test_select_make_de(self, driver, link):
        page = MmyPage(driver, link)
        page.open()
        page.select_make_is_visible()
        page.selector_dropdown_list_values_text(ExpectedData.MODEL_SELECT_DROPDOWN_TEXT[2])

    @pytest.mark.parametrize("link", Links.MMY_PAGES[21:28])
    def test_select_make_it(self, driver, link):
        page = MmyPage(driver, link)
        page.open()
        page.select_make_is_visible()
        page.selector_dropdown_list_values_text(ExpectedData.MODEL_SELECT_DROPDOWN_TEXT[3])
