import pytest
from base.data_for_test import InputDAata, ExpectedData
import time
from base.links import Links
from pages.login_page import LoginPage
from pages.my_account_page import MyAccountPage


class TestLoginPage:

    def test_user_login_15(self, driver):  # empty fields
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.click_on_login_button()
        # try:
        #     page.should_not_be_error_in_top()
        # except AssertionError:
        #     page.click_on_login_button()
        page.email_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)
        page.pass_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_user_login_2(self, driver):  # email - "test"  pass - empty
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        # time.sleep(1)
        page.put_text_email_field(InputDAata.INVALID_EMAIL)
        page.click_on_login_button()
        # try:
        #     page.should_not_be_error_in_top()
        # except AssertionError:
        #     page.click_on_login_button()
        time.sleep(2)
        page.email_error_is_correct(ExpectedData.EXPECTED_EMAIL_ERROR_INVALID)
        page.pass_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_user_login_3(self, driver):  # email - empty  pass - "!+!+!+!+!+!+!"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_pass_field(InputDAata.INVALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.email_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_user_login_4(self, driver):  # email - "test"  pass - "test"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        time.sleep(1)
        page.put_text_email_field(InputDAata.INVALID_EMAIL)
        page.put_text_pass_field(InputDAata.INVALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.email_error_is_correct(ExpectedData.EXPECTED_EMAIL_ERROR_INVALID)

    def test_user_login_5(self, driver):  # email - "test@test.com"  pass - empty
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_email_field(InputDAata.VALID_EMAIL_NOT_REGISTER)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.pass_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_user_login_6(self, driver):  # email - "test@test.com"  pass - "test"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_email_field(InputDAata.VALID_EMAIL_NOT_REGISTER)
        page.put_text_pass_field(InputDAata.INVALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.error_in_top_is_correct(ExpectedData.EXPECTED_ERROR_IN_TOP_1)

    def test_user_login_7(self, driver):  # email - "andrii.l@interactivated.me"  pass - emty
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_email_field(InputDAata.VALID_EMAIL)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.pass_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_user_login_8(self, driver):  # email - empty  pass - "Test_test"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_pass_field(InputDAata.VALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.email_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_user_login_9(self, driver):  # email - "andrii.l@interactivated.me"  pass - "test"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_email_field(InputDAata.VALID_EMAIL)
        page.put_text_pass_field(InputDAata.INVALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.error_in_top_is_correct(ExpectedData.EXPECTED_ERROR_IN_TOP_1)

    def test_user_login_10(self, driver):  # email - "test@test.com"  pass - "Test_test"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_email_field(InputDAata.VALID_EMAIL_NOT_REGISTER)
        page.put_text_pass_field(InputDAata.VALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        page.error_in_top_is_correct(ExpectedData.EXPECTED_ERROR_IN_TOP_1)

    def test_user_login_11(self, driver):  # email - "andrii.l@interactivated.me"  pass - "Test_test"
        link = Links.LOGIN_URL
        page = LoginPage(driver, link)
        page.open()
        page.put_text_email_field(InputDAata.VALID_EMAIL)
        page.put_text_pass_field(InputDAata.VALID_PASS)
        page.click_on_login_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_login_button()
        MyAccountPage(driver, Links.MY_ACCOUNT_PAGE).nav_block_is_visible()
