import time
import pytest
from selenium.webdriver.common.by import By
from base.randomaizer import Randomizer
from base.links import Links
from pages.option_page import OptionPage


class TestOptionPage:
    # @pytest.mark.smoke
    @pytest.mark.parametrize("link", Links.PRODUCT_WITH_OPTION_NO_ENG)
    def test_value_first_options(self, driver, link):
        page = OptionPage(driver, link)
        page.open()
        page.first_option().click()
        text = page.first_option().text
        page.continue_button().click()
        assert text in driver.page_source  #"All Wheel Drive (xDrive)" or text
        assert driver.current_url.split("/")[-2] != text.replace(" ", "_").lower()  # "all_wheel_drive_(xdrive)"

    @pytest.mark.parametrize("link", Links.PRODUCT_WITH_OPTION_ENG)
    def test_hreflnag_and_canonical_options(self, driver, link):
        page = OptionPage(driver, link)
        page.open()
        page.is_hreflag_present()
        first_canonical_href = page.is_canonical_present().get_attribute("href")
        url = driver.current_url
        page.continue_button().click()
        is_not_hreflang_present = page.is_not_hreflang_present()
        second_canonical_href = page.is_canonical_present().get_attribute("href")
        assert url == first_canonical_href == second_canonical_href
        assert is_not_hreflang_present == True

    @pytest.mark.parametrize("link", Links.PRODUCT_WITH_OPTION_ENG)
    def test_random_selecting_options(self, driver, link):
        page = OptionPage(driver, link)
        page.open()
        options_list = []
        i = 0
        while True:
            if page.are_options_present():
                x = driver.find_elements(By.CLASS_NAME, 'product-option-text')
                current_options_list = []
                for num in x:
                    current_options_list.append(num.text)
                options_list.append(current_options_list[Randomizer.random_option(current_options_list)])
                driver.find_element(By.XPATH, f"//div[text()='{options_list[i]}']").click()
                page.continue_button().click()
                i += 1
            else:
                break
        assert page.options_check(options_list)
