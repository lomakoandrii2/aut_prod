import pytest
from base.links import Links
from pages.product_page import ProductPage


class TestProductPage:


    @pytest.mark.parametrize("link", Links.PRODUCTS_NO_OPTIONS_EN)
    def test_hreflnag_and_canonical_products(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.is_hreflag_present()
        canonical_href = page.is_canonical_present().get_attribute("href")
        url = driver.current_url
        assert url == canonical_href


    @pytest.mark.parametrize("link", Links.PRODUCTS_NO_OPTIONS_IT)
    def test_breadcrumbs_products_it(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        home = page.home_is_visible()
        assert home.text == "HOME"

    @pytest.mark.parametrize("link", Links.PRODUCT_FOR_CART_TESTS)
    def test_success_massage(self, driver, link):
        page = ProductPage(driver, link)
        page.open()
        page.simple_is_visible().click()
        page.add_to_cart_button_is_visible().click()
        page.success_message_is_visible()

