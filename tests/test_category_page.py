import pytest
from base.links import Links
from pages.category_page import CategoryPage


class TestCategoryPage:

    #@pytest.mark.smoke
    @pytest.mark.parametrize("link", Links.CATEGORIES_IT)
    def test_breadcrumbs_categories_it(self, driver, link):
        page = CategoryPage(driver, link)
        page.open()
        home = page.home_is_visible()
        assert home.text == "HOME"