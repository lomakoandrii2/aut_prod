import pytest
from base.data_for_test import InputDAata, ExpectedData
import time
from base.links import Links
from pages.registration_page import RegistrationPage
from pages.my_account_page import MyAccountPage


class TestRegistrationPage:

    @pytest.mark.smoke
    def test_registration_all_empty_fields(self, driver):
        link = Links.REGISTRATION_PAGE
        page = RegistrationPage(driver, link)
        page.open()
        time.sleep(1)
        page.click_on_create_an_account_button()
        try:
            page.should_not_be_error_in_top()
        except AssertionError:
            page.click_on_create_an_account_button()
        page.first_name_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)
        page.last_name_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)
        page.email_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)
        page.pass_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)
        page.confirm_pass_error_is_correct(ExpectedData.REQUIRED_FIELD_ERROR)

    def test_registration_all_valid_fields(self, driver):
        link = Links.REGISTRATION_PAGE
        page = RegistrationPage(driver, link)
        page.open()
        page.put_text_first_name_field(InputDAata.INVALID_PASS)
        page.put_text_last_name_field(InputDAata.INVALID_PASS)
        page.put_text_email_field("vadim.v+1234@interactivated.me")
        page.put_text_pass_field(InputDAata.VALID_PASS)
        page.put_text_confirm_pass_field(InputDAata.VALID_PASS)
        page.click_on_create_an_account_button()
        MyAccountPage(driver, Links.MY_ACCOUNT_PAGE).nav_block_is_visible()

    def test_registration_registrated_email(self, driver): #negative - email exist
        link = Links.REGISTRATION_PAGE
        page = RegistrationPage(driver, link)
        page.open()
        page.put_text_first_name_field(InputDAata.INVALID_PASS)
        page.put_text_last_name_field(InputDAata.INVALID_PASS)
        page.put_text_email_field(InputDAata.VALID_EMAIL)
        page.put_text_pass_field(InputDAata.VALID_PASS)
        page.put_text_confirm_pass_field(InputDAata.VALID_PASS)
        page.click_on_create_an_account_button()
        assert ExpectedData.EXPECTED_ERROR_IN_TOP_2 == page.error_in_top_is_visible().text


